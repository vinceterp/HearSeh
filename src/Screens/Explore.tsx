import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { View } from 'react-native';
import MapView from 'react-native-maps';
import styles from '../../css/styles';
let fn= (args:any) => {return <></>};

let Explore: typeof fn;
export default Explore= (props) => {
    const isMobile= true;
    return (
        <View style={styles.container}>
            <StatusBar style='auto'/>
            {(isMobile) && <MapView style={styles.map}/>}
        </View>
    );
}